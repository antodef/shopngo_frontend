'use strict';

// Define the `shopngoApp` module
angular.module('shopngoApp', [
  'ngAnimate',
  'ngRoute',
  'core',
  'makeNewBuyComponents',
  'processBuyComponents',
  'presentationComponents'
]);
