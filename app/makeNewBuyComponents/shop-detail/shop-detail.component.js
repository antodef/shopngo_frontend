angular.
  module('makeNewBuyComponents.shopDetail').
  component('shopDetail', {
    templateUrl: 'makeNewBuyComponents/shop-detail/shop-detail.template.html',
    controller: ['$routeParams', 'Shop','Buy',
      function ShopDetailController($routeParams, Shop, Buy) {

        var self = this;
        self.getShopError = false;
        self.selectShopError = false;

        if (Buy.getStatus() == 200) {
          self.buy = Buy.getBuy();
          Shop.getShop($routeParams.shopId).then(
            function callback(response) {
              self.shop = response.data;
              self.status = response.status;
              self.getShopError = false;
            }, function errorCallback(response){
              self.shop = "Punto vendita non trovato!";
              self.status = 404;
              self.getShopError = true;

            }
          );
        }
        //console.log(this.buy);
        //console.log('buy id = '+Buy.getBuyId());
        //console.log('shop id = '+$routeParams.shopId);

        self.selectShop = function(){
          Buy.selectShop($routeParams.shopId);
          if (Buy.getStatus() == 200) {
            console.log(Buy.getStatus());
            self.buy = Buy.getBuy();
            self.selectShopError = false;
            window.location.replace('#!/buys/'+Buy.getBuyId()+'/'+$routeParams.shopId);
          }
          else{
            self.selectShopError = true;
          }
        };

        self.comeBack = function() {
          window.location.replace('#!/shops');
        };

        self.cancelBuy = function(){
          Buy.cancelBuy().then(
            function (response){
                window.location.replace('#!/dashboard');
            }, function(response){
              self.status = response.status;
              self.cancelBuyError = true;
            }
          );
        }
      }
    ]
  });
