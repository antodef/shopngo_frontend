'use strict';

// Register `shopList` component, along with its associated controller and template
angular.
  module('makeNewBuyComponents.timeList').
  component('timeList', {
    templateUrl: 'makeNewBuyComponents/time-list/time-list.template.html',
    controller: ['Shop', 'Buy',
      function TimeListController(Shop, Buy) {

        var self = this;
        self.getTimeListError = false;
        self.selectPickUpTimeError = false;
        self.pickUpTime;

        if (Buy.getStatus() == 200) {
          Shop.getTimeList().then(
            function callback(response) {
              self.times = response.data;
              self.status = response.status;
              self.getTimeListError = false;
              console.log(self.times);
             }, function errorCallback(){
              self.status = response.status;
              self.getTimeListError = true;
            });
        }

        self.selectPickUpTime = function(pickUpTime) {
            console.log("Select pick up time Scope!");
            console.log("Hai selezionato : "+pickUpTime);
            
            //window.location.replace('#!/payment');
            
            Buy.selectPickUpTime(pickUpTime).then(
                function callback(response) {
                    if(Buy.getStatus() == 200){
                        console.log("Time selezionato");
                        self.selectPickUpTimeError = false;
                        window.location.replace('#!/payment');
                    }
                    else{
                        self.selectPickUpTimeError = true;
                        self.status = Buy.getStatus();
                    }
                }, function errorCallback(response){
                    console.log("Time NON selezionato");
                    self.selectPickUpTimeError = false;
            });
        };

        self.comeBack = function() {
            Buy.inProgressBuy().then(
                function(response){
                    if(Buy.getStatus() == 200 ){
                        window.location.replace('#!/summary');
                    }
                    else{
                        console.log("Errore Torna indietro aggiornamento spesa");
                    }
                }
            );
          
        };

        self.cancelBuy = function(){
          Buy.cancelBuy().then(
            function (response){
                window.location.replace('#!/dashboard');
            }, function(response){
              self.status = response.status;
              self.cancelBuyError = true;
            }
          );
        };
    }]
});