'use strict';

// Register `shopList` component, along with its associated controller and template
angular.
  module('makeNewBuyComponents.shopList').
  component('shopList', {
    templateUrl: 'makeNewBuyComponents/shop-list/shop-list.template.html',
    controller: ['Shop', 'Buy',
      function ShopListController(Shop, Buy) {

        var self = this;
        self.searchShopError = false;

        if (Buy.getStatus() == 200) {
          Shop.searchShop().then(
            function callback(response) {
              self.shops = response.data;
              self.status = response.status;
              self.searchShopError = false;
          }, function errorCallback(){
              self.searchShopError = true;
          });
        }

        self.comeBack = function(){
          window.location.replace('#!/dashboard');
        }

        self.cancelBuy = function(){
          Buy.cancelBuy().then(
            function (response){
              //if (Buy.getStatus() == 200) {
                window.location.replace('#!/dashboard');
              //}
            }, function(response){
              self.status = response.status;
              self.cancelBuyError = true;
            }
          );
          //window.location.replace('#!/dashboard');
        }
        
      }
    ]
});