'use strict';

// Define the `shopList` module
angular.module('makeNewBuyComponents.buySummary', ['core.shop', 'core.buy']);