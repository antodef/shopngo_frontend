angular.
  module('makeNewBuyComponents.buySummary').
  component('buySummary', {
    templateUrl: 'makeNewBuyComponents/buy-summary/buy-summary.template.html',
    controller: [ 'Buy',
      function BuySummaryController(Buy) {

        var self = this;
        self.buyLineItems = null;
        self.totalPrice;
        self.cancelBuyError = false;
        self.closeBuyError = false;

        if (Buy.getStatus() == 200) {
          self.buy = Buy.getBuy();
          Buy.getBuyLineItems().then(
            function callback(response) {
              self.buyLineItems = response.data;
              if(response.status == 200){
                self.totalPrice = self.getTotalPrice();
                self.getShopError = false;
              }
              else{
                self.getShopError = true;
              }
            }            
          );
        }

        self.getTotalPrice = function () {
          var total = 0;
          self.buyLineItems.forEach(function (buyLineItem) {
            total += buyLineItem.product.price * buyLineItem.quantity;
          });
          return total;
        };

        self.closeBuy = function(){
          Buy.closeBuy().then(
            function (response){              
              self.closeBuyError = false;
              window.location.replace('#!/endtransaction');
            }, function(response){
              self.status = response.status;
              self.closeBuyError = true;
            }
          );
        };

        self.comeBack = function() {
            window.location.replace('#!/payment');          
        };

        self.cancelBuy = function(){
          Buy.cancelBuy().then(
            function (response){
                window.location.replace('#!/dashboard');
            }, function(response){
              self.status = response.status;
              self.cancelBuyError = true;
            }
          );
        };
          
      }
  ]});