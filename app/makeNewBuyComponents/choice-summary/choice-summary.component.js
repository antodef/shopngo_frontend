angular.
  module('makeNewBuyComponents.choiceSummary').
  component('choiceSummary', {
    templateUrl: 'makeNewBuyComponents/choice-summary/choice-summary.template.html',
    controller: [ 'Buy',
      function ChoiceSummaryController(Buy) {

        var self = this;
        self.buyLineItems = null;
        self.totalPrice;
        self.cancelBuyError = false;
        self.confirmBuyError = false;

        if (Buy.getStatus() == 200) {
          self.buy = Buy.getBuy();
          Buy.getBuyLineItems().then(
            function callback(response) {
              self.buyLineItems = response.data;
              if(response.status == 200){
                self.totalPrice = self.getTotalPrice();
                self.getShopError = false;
              }
              else{
                self.getShopError = true;
              }
            }            
          );
        }

        self.getTotalPrice = function () {
          var total = 0;
          self.buyLineItems.forEach(function (buyLineItem) {
            total += buyLineItem.product.price * buyLineItem.quantity;
          });
          return total;
        };

        self.comeBack = function() {
          window.location.replace('#!/buys/'+Buy.getBuyId()+'/'+Buy.getShopId());
        };

        self.cancelBuy = function(){
          Buy.cancelBuy().then(
            function (response){
                window.location.replace('#!/dashboard');
            }, function(response){
              self.status = response.status;
              self.cancelBuyError = true;
            }
          );
        }

        self.confirmBuy = function(){
          Buy.confirmBuy().then(
            function (response){              
              self.confirmBuyError = false;
              window.location.replace('#!/time');
            }, function(response){
              self.status = response.status;
              self.confirmBuyError = true;
            }
          );
        }
          
      }
  ]});