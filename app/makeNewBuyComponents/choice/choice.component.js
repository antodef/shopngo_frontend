angular.
  module('makeNewBuyComponents.choice').
  component('choice', {
    templateUrl: 'makeNewBuyComponents/choice/choice.template.html',
    controller: ['$routeParams', 'Buy', 'Shop','$scope',
      function ChoiceController($routeParams, Buy, Shop, $scope) {
        console.log('Vista ChoiceController');
        console.log('CHOICE ID BUY = '+ $routeParams.buyId);
        console.log('CHOICE ID SHOP = '+ $routeParams.shopId);

        var self = this;
        self.cart = [];
        self.value = 1;
        self.getCategoriesError = false;
        self.getCategoryProductsError = false;
        self.getProductDetailsError = false;
        self.endBuyError = false;
        $scope.showModal = false;

        if (Buy.getStatus() == 200) {
          this.buy = Buy.getBuy();
          console.log(this.buy);
          Shop.getCategories().then(
            function callback(response) {
              self.categories = response.data;
              self.status = response.status;
              self.getCategoriesError = false;
              Buy.getBuyLineItems().then(
                function callback(response) {
                  self.cart = response.data;
                  if(response.status == 200){
                    self.getShopError = false;
                  }
                  else{
                    self.getShopError = true;
                  }
                }            
              );
              console.log(self.categories);
              console.log(self.status);
            } , function errorCallback(response){
              self.categories = "Impossibile contattare il server";
              self.status = 404;
              self.getCategoriesError = true;
            }
          );
        }


        self.getCategoryProducts = function(categoryId, category){
          console.log("id categoria selezionata: "+categoryId);
          console.log("id shop selezionato: "+this.buy.id);
          self.selectedCategoryId = categoryId;
          Shop.getCategoryProducts(categoryId).then(
            function callback(response) {
              self.products = response.data;
              self.subcategories = category.subCategories;
              self.status = response.status;
              self.getCategoryProductsError = false;
              console.log(self.products);
              console.log(self.status);
            } , function errorCallback(response){
              self.categories = "Impossibile contattare il server";
              self.status = 404;
              self.getCategoryProductsError = true;
            }
          );
        };

        self.getSubcategoryProducts = function(subcategoryId){
          console.log("id categoria selezionata: "+subcategoryId);
          console.log("id shop selezionato: "+this.buy.id);
          Shop.getSubcategoryProducts(self.selectedCategoryId,subcategoryId).then(
            function callback(response) {
              self.products = response.data;
              self.status = response.status;
              self.getCategoryProductsError = false;
              console.log(self.products);
              console.log(self.status);
            } , function errorCallback(response){
              self.categories = "Impossibile contattare il server";
              self.status = 404;
              self.getCategoryProductsError = true;
            }
          );
        };


        $scope.toggleModal = function(product){
            $scope.showModal = !$scope.showModal;
            if($scope.showModal){
              console.log($scope.showModal);
              console.log(product);
              Shop.getProductDetail($routeParams.shopId, product.id).then(
                function callback(response) {
                  self.product = response.data;
                  self.status = response.status;
                  self.getProductDetailsError = false;
                  console.log(self.product);
                  console.log(self.status);
                } , function errorCallback(response){
                  self.categories = "Impossibile contattare il server";
                  self.status = 404;
                  self.getProductDetailsError = true;
                }
              );
              if(self.cart.length == 0){
                self.value = 1;
                self.max_value = product.stock;
              }else{
                console.log("carrello non vuoto");
                self.cart.forEach(function (item) {
                  console.log(item);
                  console.log(product);
                  if (item.product.id === product.id) {
                    console.log(self.cart.length);
                    self.value = item.quantity;
                    console.log(self.product);
                    //self.max_value = item.quantity + self.product.quantity;
                  }
                  else{
                    console.log(self.cart.length);
                    self.value = 1;
                    self.max_value = product.stock;
                  }
                }); 
              }
                           
            }
        };

        
        self.addProduct = function (product,selection) {
          console.log("addProduct Scope!");
          Buy.addProduct(product,selection).then(
            function successCallback(response) {
              self.buyLineItem = response.data;
              self.status = response.status;
              console.log(response.data);
              console.log(response.status);
              console.log(self.buyLineItem.id);
              console.log(self.buyLineItem.product.id);
              
              var found = false;
              self.cart.forEach(function (item) {
                if (item.id === self.buyLineItem.id) {
                  item.quantity=selection;
                  found = true;
                }
              });
              if (!found) {
                self.cart.push(self.buyLineItem);
                //console.log(product);
                //self.cart.push(self.bli);
              }
          });
          $scope.toggleModal(product);              
        };

        self.getCartPrice = function () {
          var total = 0;
          self.cart.forEach(function (buyLineItem) {
            total += buyLineItem.product.price * buyLineItem.quantity;
          });
          return total;
        };

        self.removeProduct = function (index, buyLineItemId) {
          console.log("removeProduct Scope!"+index);
          
          Buy.removeProduct(buyLineItemId).then(
            function callback(response) {
              self.listproduct = response.data;
              self.status = response.status;

              console.log(index);
              self.cart.splice(index, 1);
          });          
        }

        self.endBuy = function(){
          console.log("endBuy Scope");
          /*Buy.endBuy().then(
            function successCallback(response){
              self.endBuyError = false;
              console.log(response.data);
              console.log(response.status);
              window.location.replace("#!/summary");
            }, function errorCallback(response){
              self.endBuyError = true;
            }
          );*/
          if(Buy.endBuy() != null){
            console.log(Buy.endBuy());
            window.location.replace("#!/summary");
          }
        };

        self.comeBack = function() {
          window.location.replace('#!/shops');
        };

        self.cancelBuy = function(){
          Buy.cancelBuy().then(
            function (response){
                window.location.replace('#!/dashboard');
            }, function(response){
              self.status = response.status;
              self.cancelBuyError = true;
            }
          );
        }

      }
    ]
  }).directive('productDetail', function () {
    return {
      template: '<div class="modal fade">' + 
          '<div class="modal-dialog">' + 
            '<div class="modal-content">' + 
              '<div class="modal-header">' + 
                '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>' + 
                '<h4 class="modal-title">{{ title }}</h4>' + 
              '</div>' + 
              '<div class="modal-body" ng-transclude></div>' + 
            '</div>' + 
          '</div>' + 
        '</div>',
      restrict: 'E',
      transclude: true,
      replace:true,
      scope:true,
      link: function postLink(scope, element, attrs) {
        scope.title = attrs.title;

        scope.$watch(attrs.visible, function(value){
          if(value == true)
            $(element).modal('show');
          else
            $(element).modal('hide');
        });

        $(element).on('shown.bs.modal', function(){
          scope.$apply(function(){
            scope.$parent[attrs.visible] = true;
          });
        });

        $(element).on('hidden.bs.modal', function(){
          scope.$apply(function(){
            scope.$parent[attrs.visible] = false;
          });
        });
      }
    };
  });