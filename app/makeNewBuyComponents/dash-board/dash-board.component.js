angular.
  module('makeNewBuyComponents.dashBoard').
  component('dashBoard', {
    templateUrl: 'makeNewBuyComponents/dash-board/dash-board.template.html',
    controller: [ 'Buy', 
      function DashBoardController( Buy ) {
        var self = this;
        self.user = 'Salvatore';
        self.newBuyError = false;

        self.makeNewBuy = function(){
          Buy.makeNewBuy().then(function (response){
            if (Buy.getStatus() == 201) {
              window.location.replace('#!/shops');
              self.newBuyError = false;
            }
            else {
              self.newBuyError = true;
            }
          })
        }

        self.forceMakeNewBuy = function(){
          Buy.deleteCurrentBuy().then(function (response){
            if (Buy.getStatus() == 200) {
              Buy.makeNewBuy().then(function (response){
                if (Buy.getStatus() == 201) {
                  window.location.replace('#!/shops');
                  self.newBuyError = false;
                }
                else {
                  self.newBuyError = true;
                }
              })
            }
            else {
              self.newBuyError = true;
            }
          })
        }

        self.comeBack = function(){
          window.location.replace('#!/home');
        }
      }
    ]
  });