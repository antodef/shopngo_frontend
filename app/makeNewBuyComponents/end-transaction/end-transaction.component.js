angular.
  module('makeNewBuyComponents.endTransaction').
  component('endTransaction', {
    templateUrl: 'makeNewBuyComponents/end-transaction/end-transaction.template.html',
    controller: [ 'Buy',
      function EndTransactionController(Buy) {

        var self = this;

        self.comeToDashboard = function(){
          window.location.replace('#!/dashboard');
        }
          
      }
  ]});