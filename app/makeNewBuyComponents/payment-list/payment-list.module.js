'use strict';

// Define the `shopList` module
angular.module('makeNewBuyComponents.paymentList', ['core.shop', 'core.buy']);