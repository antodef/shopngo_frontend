'use strict';

// Register `shopList` component, along with its associated controller and template
angular.
  module('makeNewBuyComponents.paymentList').
  component('paymentList', {
    templateUrl: 'makeNewBuyComponents/payment-list/payment-list.template.html',
    controller: ['Shop', 'Buy',
      function PaymentListController(Shop, Buy) {

        var self = this;
        self.getPaymentListError = false;
        self.selectPaymentError = false;
        self.payment;

        if (Buy.getStatus() == 200) {
          Shop.getPaymentList().then(
            function callback(response) {
              self.payments = response.data;
              self.status = response.status;
              self.getPaymentListError = false;
              console.log(self.payments);
             }, function errorCallback(response){
              self.status = response.status;
              self.getPaymentListError = true;
            });
        }

        self.selectPayment = function(payment) {
            console.log("Select payment up time Scope!");
            console.log("Hai selezionato : "+payment);
            
            Buy.selectPayment(payment).then(
                function callback(response) {
                  if(Buy.getStatus() == 200){
                    console.log("Payment selezionato");
                    self.selectPaymentError = false;
                    window.location.replace('#!/totalsummary');
                  }
                  else{
                    self.selectPaymentError = true;
                    self.status = Buy.getStatus();
                  }
                });
        };

        self.comeBack = function() {
            window.location.replace('#!/time');          
        };

        self.cancelBuy = function(){
          Buy.cancelBuy().then(
            function (response){
                window.location.replace('#!/dashboard');
            }, function(response){
              self.status = response.status;
              self.cancelBuyError = true;
            }
          );
        };
    }]
});