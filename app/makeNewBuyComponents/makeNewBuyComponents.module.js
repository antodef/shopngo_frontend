angular
  .module('makeNewBuyComponents', [
    'makeNewBuyComponents.shopDetail',
    'makeNewBuyComponents.shopList',
    'makeNewBuyComponents.dashBoard',
    'makeNewBuyComponents.newBuy',
    'makeNewBuyComponents.choice',
    'makeNewBuyComponents.choiceSummary',
    'makeNewBuyComponents.timeList',
    'makeNewBuyComponents.paymentList',
    'makeNewBuyComponents.buySummary',
    'makeNewBuyComponents.endTransaction'
]);