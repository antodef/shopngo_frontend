angular.
  module('makeNewBuyComponents.newBuy').
  component('newBuy', {
    templateUrl: 'makeNewBuyComponents/new-buy/new-buy.template.html',
    controller: ['Buy',
      function newBuyController(Buy) {
        var self = this;
        self.newBuyError = false;
        if (Buy.getStatus() == 201) {
          window.location.replace('#!/shops');
          self.newBuyError = false;
        }
        else {
          window.location.replace('#!/dashboard');
          window.alert("Hai già una spesa in esecuzione!");
        }
      }
    ]
  });

