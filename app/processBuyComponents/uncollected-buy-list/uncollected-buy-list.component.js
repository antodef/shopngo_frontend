'use strict';

// Register `shopList` component, along with its associated controller and template
angular.
  module('processBuyComponents.uncollectedBuyList').
  component('uncollectedBuyList', {
    templateUrl: 'processBuyComponents/uncollected-buy-list/uncollected-buy-list.template.html',
    controller: ['Process', '$scope','$timeout',
      function UncollectedBuyListController(Process, $scope,$timeout) {

        var self = this;
        self.buys = [];
        self.getUncollectedBuysError = false;
        self.getUncollectedBuyError = false;
        $scope.showModal = false;

        if( Process.getStatus()==200 ){
          console.log('UncollectedBuys Controller Scope');
          console.log(Process.getStatus());
          self.buys = Process.getRequestedUncollectedBuys();
          console.log(self.buys);
          self.getUncollectedBuysError = false;
        }
        else{
          self.getUncollectedBuysError = true;
        }

        self.comeBack = function(){
          window.location.replace('#!/operator');
        }

        self.toggleModal = function(buyId){
            $scope.showModal = !$scope.showModal;
            if($scope.showModal){
              console.log($scope.showModal);
              console.log(buyId);
              Process.getRequestedBuy(buyId).then(
                function callback(response) {
                  console.log('Success callback scope');
                  if( Process.getStatus()==200 ){
                    self.buy = Process.getBuy();
                    console.log(self.buy);
                    self.getUncollectedBuyError = false;
                  }
                  else{
                    self.getUncollectedBuyError = true;
                  }
                }
              );              
            }
        };

      }
    ]
}).directive('uncollectedBuyDetail', function () {
    return {
      template: '<div class="modal fade">' + 
          '<div class="modal-dialog">' + 
            '<div class="modal-content">' + 
              '<div class="modal-header">' + 
                '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>' + 
                '<h3 class="modal-title">{{ title }}</h3>' + 
              '</div>' + 
              '<div class="modal-body" ng-transclude></div>' + 
            '</div>' + 
          '</div>' + 
        '</div>',
      restrict: 'E',
      transclude: true,
      replace:true,
      scope:true,
      link: function postLink(scope, element, attrs) {
        scope.title = attrs.title;

        scope.$watch(attrs.visible, function(value){
          if(value == true)
            $(element).modal('show');
          else
            $(element).modal('hide');
        });

        $(element).on('shown.bs.modal', function(){
          scope.$apply(function(){
            scope.$parent[attrs.visible] = true;
          });
        });

        $(element).on('hidden.bs.modal', function(){
          scope.$apply(function(){
            scope.$parent[attrs.visible] = false;
          });
        });
      }
    };
  });