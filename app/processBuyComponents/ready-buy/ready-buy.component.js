'use strict';

// Register `shopList` component, along with its associated controller and template
angular.
  module('processBuyComponents.readyBuy').
  component('readyBuy', {
    templateUrl: 'processBuyComponents/ready-buy/ready-buy.template.html',
    controller: ['Shop', 'Buy', 'Process', '$scope',
      function ProcessBuyController(Shop, Buy, Process, $scope) {

        var self = this;

        self.comeToDashboard = function(){
          window.location.replace('#!/operator');
        }

      }
    ]
});