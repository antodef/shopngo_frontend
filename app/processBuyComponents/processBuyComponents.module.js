angular
  .module('processBuyComponents', [
    'processBuyComponents.operatorDashBoard',
    'processBuyComponents.shopBuyList',
    'processBuyComponents.operatorBuyList',
    'processBuyComponents.readyBuyList',
    'processBuyComponents.processBuy',
    'processBuyComponents.readyBuy',
    'processBuyComponents.finishedBuyList',
    'processBuyComponents.uncollectedBuyList'
]);