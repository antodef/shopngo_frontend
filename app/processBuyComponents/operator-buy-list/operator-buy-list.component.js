'use strict';

// Register `shopList` component, along with its associated controller and template
angular.
  module('processBuyComponents.operatorBuyList').
  component('operatorBuyList', {
    templateUrl: 'processBuyComponents/operator-buy-list/operator-buy-list.template.html',
    controller: ['Shop', 'Buy', 'Process', '$scope','$timeout',
      function OperatorBuyListController(Shop, Buy, Process, $scope,$timeout) {

        var self = this;
        self.getAssignedBuysError = false;
        self.getAssignedBuysError = false;
        self.refuseBuyError = false;
        self.refuseBuySuccess = false;
        self.processBuyError = false;
        $scope.showModal = false;

        if( Process.getStatus()==200 ){
          console.log('AssignedBuys Controller Scope');
          console.log(Process.getStatus());
          self.buys = Process.getRequestedAssignedBuys();
          console.log(self.buys);
          self.getAssignedBuysError = false;
        }
        else{
          self.getAssignedBuysError = true;
        }

        self.comeBack = function(){
          window.location.replace('#!/operator');
        }

        self.toggleModal = function(buyId){
            $scope.showModal = !$scope.showModal;
            if($scope.showModal){
              console.log($scope.showModal);
              console.log(buyId);
              Process.getRequestedBuy(buyId).then(
                function callback(response) {
                  console.log('Success callback scope');
                  if( Process.getStatus()==200 ){
                    self.buy = Process.getBuy();
                    console.log(self.buy);
                    self.getAssignedBuyError = false;
                  }
                  else{
                    self.getAssignedBuyError = true;
                  }
                }
              );              
            }
        };
        
        self.refuseBuy = function(buyId){
          console.log('Refuse buy Scope');
          Process.refuseAndRefresh(buyId)
            .then(function(response){
              console.log('Callback scope');
              if( Process.getStatus()==200 ){
                self.refuseBuyError = false;
                self.refuseBuySuccess = true;
                self.buys = Process.getRequestedAssignedBuys();
                console.log(self.buys);
                self.toggleModal(buyId);
              }
              else{
                self.refuseBuyError = true;
                self.refuseBuySuccess = false;
              }
            });
        };

        self.processBuy = function(buyId){
          console.log('Process Buy Scope');
          Process.processAndRefresh(buyId)
            .then(function(response){
              console.log('Callback scope');
            if( Process.getStatus()==200 ){
              self.toggleModal(buyId);
              self.processBuyError = false;
              $timeout(function(){window.location.replace('#!/process-buy')},5000);
            }
            else{
              self.processBuyError = true;
            }
          })
        };

      }
    ]
}).directive('assignedBuyDetail', function () {
    return {
      template: '<div class="modal fade">' + 
          '<div class="modal-dialog">' + 
            '<div class="modal-content">' + 
              '<div class="modal-header">' + 
                '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>' + 
                '<h3 class="modal-title">{{ title }}</h3>' + 
              '</div>' + 
              '<div class="modal-body" ng-transclude></div>' + 
            '</div>' + 
          '</div>' + 
        '</div>',
      restrict: 'E',
      transclude: true,
      replace:true,
      scope:true,
      link: function postLink(scope, element, attrs) {
        scope.title = attrs.title;

        scope.$watch(attrs.visible, function(value){
          if(value == true)
            $(element).modal('show');
          else
            $(element).modal('hide');
        });

        $(element).on('shown.bs.modal', function(){
          scope.$apply(function(){
            scope.$parent[attrs.visible] = true;
          });
        });

        $(element).on('hidden.bs.modal', function(){
          scope.$apply(function(){
            scope.$parent[attrs.visible] = false;
          });
        });
      }
    };
  });