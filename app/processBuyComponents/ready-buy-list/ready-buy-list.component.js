'use strict';

// Register `shopList` component, along with its associated controller and template
angular.
  module('processBuyComponents.readyBuyList').
  component('readyBuyList', {
    templateUrl: 'processBuyComponents/ready-buy-list/ready-buy-list.template.html',
    controller: ['Shop', 'Buy', 'Process', '$scope',
      function ReadyBuyListController(Shop, Buy, Process, $scope) {

        var self = this;
        self.getReadyBuysError = false;
        self.getReadyBuyError = false;
        self.finishBuyError = false;
        self.finishBuySuccess = false;
        self.uncollectedBuyError = false;
        self.uncollectedBuySuccess = false;
        self.totalPrice;
        $scope.showModal = false;

        if( Process.getStatus()==200 ){
          console.log('ReadyBuys Controller Scope');
          console.log(Process.getStatus());
          self.buys = Process.getRequestedReadyBuys();
          console.log(self.buys);
          self.getReadyBuysError = false;
        }
        else{
          self.getReadyBuysError = true;
        }

        self.comeBack = function(){
          window.location.replace('#!/operator');
        }

        self.toggleModal = function(buyId){
            $scope.showModal = !$scope.showModal;
            if($scope.showModal){
              console.log($scope.showModal);
              console.log(buyId);
              Process.getRequestedBuy(buyId).then(
                function callback(response) {
                  console.log('Success callback scope');
                  if( Process.getStatus()==200 ){
                    self.buy = Process.getBuy();
                    self.totalPrice = self.getTotalPrice(self.buy);
                    console.log(self.buy);
                    self.getReadyBuyError = false;
                  }
                  else{
                    self.getReadyBuyError = true;
                  }
                }
              );              
            }
        };
        
        self.finishBuy = function(buyId){
          console.log('FinishBuy Scope');
          Process.finishAndRefresh(buyId)
            .then(function(response){
              console.log('Callback scope');
              if( Process.getStatus()==200 ){
                self.finishBuyError = false;
                self.finishBuySuccess = true;
                self.buys = Process.getRequestedReadyBuys();
                console.log(self.buys);
                self.toggleModal(buyId);
              }
              else{
                self.finishBuyError = true;
                self.finishBuySuccess = false;
              }
            });
        };

        self.uncollectedBuy = function(buyId){
          console.log('uncollectedBuy Scope');
          Process.uncollectedAndRefresh(buyId)
             .then(function(response){
              console.log('Callback scope');
              if( Process.getStatus()==200 ){
                self.uncollectedBuyError = false;
                self.uncollectedBuySuccess = true;
                self.buys = Process.getRequestedReadyBuys();
                console.log(self.buys);
                self.toggleModal(buyId);
              }
              else{
                self.uncollectedBuyError = true;
                self.uncollectedBuySuccess = false;
              }
            });
        };

        self.getTotalPrice = function (buy) {
          var total = 0;
          self.buy.buyLineItems.forEach(function (buyLineItem) {
            total += buyLineItem.product.price * buyLineItem.quantity;
          });
          return total;
        };

      }
    ]
}).directive('readyBuyDetail', function () {
    return {
      template: '<div class="modal fade">' + 
          '<div class="modal-dialog">' + 
            '<div class="modal-content">' + 
              '<div class="modal-header">' + 
                '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>' + 
                '<h3 class="modal-title">{{ title }}</h3>' + 
              '</div>' + 
              '<div class="modal-body" ng-transclude></div>' + 
            '</div>' + 
          '</div>' + 
        '</div>',
      restrict: 'E',
      transclude: true,
      replace:true,
      scope:true,
      link: function postLink(scope, element, attrs) {
        scope.title = attrs.title;

        scope.$watch(attrs.visible, function(value){
          if(value == true)
            $(element).modal('show');
          else
            $(element).modal('hide');
        });

        $(element).on('shown.bs.modal', function(){
          scope.$apply(function(){
            scope.$parent[attrs.visible] = true;
          });
        });

        $(element).on('hidden.bs.modal', function(){
          scope.$apply(function(){
            scope.$parent[attrs.visible] = false;
          });
        });
      }
    };
  });