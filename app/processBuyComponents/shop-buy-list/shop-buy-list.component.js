'use strict';

// Register `shopList` component, along with its associated controller and template
angular.
  module('processBuyComponents.shopBuyList').
  component('shopBuyList', {
    templateUrl: 'processBuyComponents/shop-buy-list/shop-buy-list.template.html',
    controller: ['Shop', 'Buy', 'Process', '$scope',
      function ShopBuyListController(Shop, Buy, Process, $scope) {

        var self = this;
        self.buy = {};
        self.buys = [];
        self.status = null;
        self.shopBuyListError = false;
        $scope.showModal = false;

        if (Process.getStatus() == 200) {
          console.log(Process.getStatus());
          console.log('ShopBuyList Controller Scope');
          self.buys = Process.getRequestedClosedBuys();
          console.log(self.buys);
          self.shopBuyListError = false;
        }
        else{
          self.shopBuyListError = true;
        }

        self.comeBack = function(){
          window.location.replace('#!/operator');
        }

        self.toggleModal = function(index,buyId){
            $scope.showModal = !$scope.showModal;
            console.log('ToggleModal Scope');
            console.log(index);
            if($scope.showModal){
              console.log($scope.showModal);
              console.log(buyId);
              Process.getRequestedBuy(buyId).then(
                function callback(response) {
                  console.log('Success callback scope');
                  if( Process.getStatus()==200 ){
                    self.buy = Process.getBuy();
                    console.log(self.buy);
                    self.getClosedBuyError = false;
                  }
                  else{
                    self.getClosedBuyError = true;
                  }
                }
              );              
            }
        };

        self.assignBuy = function(index,buyId){
          console.log('Assign and refresh Scope');
          Process.assignAndRefresh(buyId).then(
            function(response){
              console.log('Callback scope');
              if( Process.getStatus()==200){
                self.assignBuyError = false;
                self.assignBuySuccess = true;
                self.buys = Process.getRequestedClosedBuys();
                console.log(self.buys);
                self.toggleModal(buyId);
              }
              else{
                self.assignBuyError = true;
                self.assignBuySuccess = false;
              }
            });
        };
        
      }
    ]
}).directive('buyDetail', function () {
    return {
      template: '<div class="modal fade">' + 
          '<div class="modal-dialog">' + 
            '<div class="modal-content">' + 
              '<div class="modal-header">' + 
                '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>' + 
                '<h3 class="modal-title">{{ title }}</h3>' + 
              '</div>' + 
              '<div class="modal-body" ng-transclude></div>' + 
            '</div>' + 
          '</div>' + 
        '</div>',
      restrict: 'E',
      transclude: true,
      replace:true,
      scope:true,
      link: function postLink(scope, element, attrs) {
        scope.title = attrs.title;

        scope.$watch(attrs.visible, function(value){
          if(value == true)
            $(element).modal('show');
          else
            $(element).modal('hide');
        });

        $(element).on('shown.bs.modal', function(){
          scope.$apply(function(){
            scope.$parent[attrs.visible] = true;
          });
        });

        $(element).on('hidden.bs.modal', function(){
          scope.$apply(function(){
            scope.$parent[attrs.visible] = false;
          });
        });
      }
    };
  });