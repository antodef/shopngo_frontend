'use strict';

// Register `shopList` component, along with its associated controller and template
angular.
  module('processBuyComponents.processBuy').
  component('processBuy', {
    templateUrl: 'processBuyComponents/process-buy/process-buy.template.html',
    controller: ['Shop', 'Buy', 'Process', '$scope',
      function ProcessBuyController(Shop, Buy, Process, $scope) {

        var self = this;
        self.buy = {};
        self.bliToDo = [];
        self.bliDone = [];

        if( Process.getStatus()==200 ){
            console.log('ProcessBuyController Scope');
            self.buy = Process.getBuy();
            self.bliToDo = self.buy.buyLineItems;
            console.log(self.buy);
            self.processBuyError = false;
        }
        else{
            self.processBuyError = true;
        }

        self.comeBack = function(buyId){
          Process.assignAndRefresh(buyId)
            .then(function(response){
              if( Process.getStatus()==200 ){
                self.comeBackError = false;
                window.location.replace('#!/operator-buy-list');
              }
              else{
                self.comeBackError = true;
              }
          });
        }

        self.readyBuy = function(buyId){
          console.log('Ready buy Scope');
          Process.readyBuy(buyId)
            .then(function(response){
              if( Process.getStatus()==200 ){
                self.readyBuyError = false;
                window.location.replace('#!/ready-buy');
              }
              else{
                self.readyBuyError = true;
              }
            });
        }

        self.setDone = function(bli){
          console.log('setDone Scope');
          self.bliDone.push(bli);
          self.bliToDo.splice( self.bliToDo.indexOf(bli) , 1);
          console.log(self.bliToDo);
          console.log(self.bliDone);
        }

        self.setUndone = function(bli){
          console.log('setUndone Scope');
          self.bliToDo.push(bli);
          self.bliDone.splice( self.bliDone.indexOf(bli) , 1);
          console.log(self.bliToDo);
          console.log(self.bliDone);
        }

      }
    ]
});