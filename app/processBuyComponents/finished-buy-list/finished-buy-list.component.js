'use strict';

// Register `shopList` component, along with its associated controller and template
angular.
  module('processBuyComponents.finishedBuyList').
  component('finishedBuyList', {
    templateUrl: 'processBuyComponents/finished-buy-list/finished-buy-list.template.html',
    controller: ['Shop', 'Buy', 'Process', '$scope','$timeout',
      function FinishedBuyListController(Shop, Buy, Process, $scope,$timeout) {

        var self = this;
        self.buys = [];
        self.getFinishedBuysError = false;
        $scope.showModal = false;

        if( Process.getStatus()==200 ){
          console.log('AssignedBuys Controller Scope');
          console.log(Process.getStatus());
          self.buys = Process.getRequestedFinishedBuys();
          console.log(self.buys);
          self.getFinishedBuysError = false;
        }
        else{
          self.getFinishedBuysError = true;
        }

        self.comeBack = function(){
          window.location.replace('#!/operator');
        }

        self.toggleModal = function(buyId){
            $scope.showModal = !$scope.showModal;
            if($scope.showModal){
              console.log($scope.showModal);
              console.log(buyId);
              Process.getRequestedBuy(buyId).then(
                function callback(response) {
                  console.log('Success callback scope');
                  if( Process.getStatus()==200 ){
                    self.buy = Process.getBuy();
                    self.totalPrice = self.getTotalPrice(self.buy);
                    console.log(self.buy);
                    self.getAssignedBuyError = false;
                  }
                  else{
                    self.getAssignedBuyError = true;
                  }
                }
              );              
            }
        };

        self.getTotalPrice = function (buy) {
          var total = 0;
          self.buy.buyLineItems.forEach(function (buyLineItem) {
            total += buyLineItem.product.price * buyLineItem.quantity;
          });
          return total;
        };

      }
    ]
}).directive('finishedBuyDetail', function () {
    return {
      template: '<div class="modal fade">' + 
          '<div class="modal-dialog">' + 
            '<div class="modal-content">' + 
              '<div class="modal-header">' + 
                '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>' + 
                '<h3 class="modal-title">{{ title }}</h3>' + 
              '</div>' + 
              '<div class="modal-body" ng-transclude></div>' + 
            '</div>' + 
          '</div>' + 
        '</div>',
      restrict: 'E',
      transclude: true,
      replace:true,
      scope:true,
      link: function postLink(scope, element, attrs) {
        scope.title = attrs.title;

        scope.$watch(attrs.visible, function(value){
          if(value == true)
            $(element).modal('show');
          else
            $(element).modal('hide');
        });

        $(element).on('shown.bs.modal', function(){
          scope.$apply(function(){
            scope.$parent[attrs.visible] = true;
          });
        });

        $(element).on('hidden.bs.modal', function(){
          scope.$apply(function(){
            scope.$parent[attrs.visible] = false;
          });
        });
      }
    };
  });