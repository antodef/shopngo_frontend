angular.
  module('processBuyComponents.operatorDashBoard').
  component('operatorDashBoard', {
    templateUrl: 'processBuyComponents/operator-dash-board/operator-dash-board.template.html',
    controller: [ 'Process', 
      function OperatorDashBoardController( Process ) {

        var self = this;
        self.user = 'Salvatore';

        self.getClosedBuys = function(){
          window.location.replace('#!/shop-buy-list');
        }

        self.getAssignedBuys = function(){
          window.location.replace('#!/operator-buy-list');
        }

        self.getReadyBuys = function(){
          window.location.replace('#!/ready-buy-list');
        }

        self.getFinishedBuys = function(){
          window.location.replace('#!/finished-buy-list');
        }

        self.getUncollectedBuys = function(){
          window.location.replace('#!/uncollected-buy-list');
        }

        self.comeBack = function(){
          window.location.replace('#!/home');
        }

      }
    ]
  });