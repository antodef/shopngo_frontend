angular.
  module('shopngoApp').
  config(['$locationProvider', '$routeProvider',
    function config($locationProvider, $routeProvider) {
      $locationProvider.hashPrefix('!');

      $routeProvider.
        when('/home', {
          template: '<home-dash-board></home-dash-board>'
        }).
        when('/dashboard', {
          template: '<dash-board></dash-board>'
        }).
        when('/shops', {
          template: '<shop-list></shop-list>',
          resolve: {
            buy: function(Buy) {
              return Buy.getOrFetchBuy();
            }
          }
        }).
        when('/shops/:shopId', {
          template: '<shop-detail></shop-detail>',
          resolve: {
            buy: function(Buy) {
              return Buy.getOrFetchBuy()
            }
          }
        }).
        when('/buys/:buyId/:shopId', {
          template: '<choice></choice>',
          resolve: {
            buy: function(Buy) {
              return Buy.getOrFetchBuy()
            }
          }
        }).
        when('/summary', {
          template: '<choice-summary></choice-summary>',
          resolve: {
            buy: function(Buy) {
              return Buy.getOrFetchBuy()
            }
          }
        }).
        when('/time', {
          template: '<time-list></time-list>',
          resolve: {
            buy: function(Buy) {
              return Buy.getOrFetchBuy()
            }
          }
        }).
        when('/payment', {
          template: '<payment-list></payment-list>',
          resolve: {
            buy: function(Buy) {
              return Buy.getOrFetchBuy()
            }
          }
        }).
        when('/totalsummary', {
          template: '<buy-summary></buy-summary>',
          resolve: {
            buy: function(Buy) {
              return Buy.getOrFetchBuy()
            }
          }
        }).
        when('/endtransaction', {
          template: '<end-transaction></end-transaction>',
          resolve: {
            buy: function(Buy) {
              return Buy.getOrFetchBuy()
            }
          }
        }).
        when('/operator', {
          template: '<operator-dash-board></operator-dash-board>'
        }).
        when('/shop-buy-list', {
          template: '<shop-buy-list></shop-buy-list>',
          resolve: {
            buys: function(Process){
              return Process.getClosedBuys()
            }
          }
        }).
        when('/operator-buy-list', {
          template: '<operator-buy-list></operator-buy-list>',
          resolve: {
            buys: function(Process){
              return Process.getAssignedBuys()
            }
          }
        }).
        when('/process-buy', {
          template: '<process-buy></process-buy>',
          resolve:{
            buys: function(Process){
              return Process.getProcessBuys();
            }
          }
        }).     
        when('/ready-buy', {
          template: '<ready-buy></ready-buy>'
        }).
        when('/ready-buy-list', {
          template: '<ready-buy-list></ready-buy-list>',
          resolve: {
            buys: function(Process){
              return Process.getReadyBuys();
            }
          }
        }).        
        when('/finished-buy-list', {
          template: '<finished-buy-list></finished-buy-list>',
          resolve: {
            buys: function(Process){
              return Process.getFinishedBuys();
            }
          }
        }).        
        when('/uncollected-buy-list', {
          template: '<uncollected-buy-list></uncollected-buy-list>',
          resolve: {
            buys: function(Process){
              return Process.getUncollectedBuys();
            }
          }
        }).
        otherwise('/home');
    }
  ]);