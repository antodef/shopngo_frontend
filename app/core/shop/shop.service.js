angular.
    module('core.shop').
    factory('Shop',
        function($http){
            
            var shop = null;

            var status = null;
            
            var ShopService = {};

            ShopService.getShop = function (shopId) {
                return $http.get('http://localhost:9090/api/shops/'+shopId);
            };

            ShopService.searchShop = function () {
                return $http.get('http://localhost:9090/api/shops/');
            };

            ShopService.getCategories = function () {
                return $http.get('http://localhost:9090/api/buy/shop/categories');
            };

            ShopService.getCategoryProducts = function (categoryId) {
                return $http.get('http://localhost:9090/api/buy/shop/products?category='+categoryId);
            };

            ShopService.getSubcategoryProducts = function (categoryId, subcategoryId) {
                return $http.get('http://localhost:9090/api/buy/shop/products?category='+categoryId+'&subcategory='+subcategoryId);
            };

            ShopService.getProductDetail = function (shopId, productId) {
                return $http.get('http://localhost:9090/api/buy/shop/products/'+productId);
            };

            ShopService.getTimeList = function () {
                console.log("Get Time List Scope!");
                return $http.get('http://localhost:9090/api/buy/shop/pickuptimes');
            };

            ShopService.getPaymentList = function () {
                console.log("Get Payment List Scope!");
                return $http.get('http://localhost:9090/api/buy/shop/payments');
            };

            return ShopService;
        });