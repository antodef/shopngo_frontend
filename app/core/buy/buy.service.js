angular.
    module('core.buy').
    factory('Buy',
        function($http,$q,$route,$routeParams){
            
            //var buy = null;
            var buy = {};

            buy.getID = function(){
                return buy.id;
            };

            var status = null;

            var bli = null;
            
            var BuyService = {};
            
            BuyService.getBuyId = function() {
                return buy.id;
            }

            BuyService.getShopId = function() {
                return buy.shop.id;
            }

            BuyService.makeNewBuy = function () {
                return $http.post('http://localhost:9090/api/buys')
                    .then(function successCallback(response) {
                        status = response.status;
                        buy = response.data;
                    }, function errorCallback(response){
                        if (response.data == null){
                            status=500;
                            buy="Server Unreachable";
                        }
                        else {
                            status = response.status;
                            buy = response.data;
                        }
                });
            };

            BuyService.deleteCurrentBuy = function () {
                return $http.delete('http://localhost:9090/api/buy')
                    .then(function successCallback(response) {
                        status = response.status;
                        buy = {};
                    }, function errorCallback(response){
                        //if (response.data == null){
                            status=500;
                            //buy="Server Unreachable";
                        //}
                        //else {
                        //    status = response.status;
                        //    buy = response.data;
                        //}
                });
            };

            BuyService.getOrFetchBuy = function() {                
                //if (buy == null) {
                if (angular.equals({},buy)) {
                    $http.get('http://localhost:9090/api/buy')
                        .then(function successCallback(response) {
                            buy = response.data;
                            status = response.status;
                            console.log("Backend loaded - Buy Charged - BuyID="+buy.id);
                            console.log(buy);
                            return buy;
                        }, function errorCallback(response){
                            if (response.data == null){
                                status=500;
                                buy="Server Unreachable";
                            }
                            else {
                                status = response.status;
                                buy = response.data;
                            }
                            return buy;
                        })
                }
                else {
                    console.log("Backend ALREADY loaded - Buy Charged - BuyID="+buy.id);
                    status = 200;
                    return buy;
                }
            }

            BuyService.selectShop = function(shop) {
                return $http.put('http://localhost:9090/api/buy/'+shop)
                    .then(function successCallback(response) {
                        buy = response.data;
                        status = response.status;
                        console.log("Buy Updated");
                        //return buy;
                    }, function errorCallback(response){
                        if (response.data == null){
                            status=500;
                            buy="Server Unreachable";
                        }
                        else {
                            status = response.status;
                            buy = response.data;
                        }
                        //return buy;
                })
            }


                        
            BuyService.addProduct = function(product,selection) {
                console.log('ADD PRODUCT scope');
                //console.log('id Buy SERVICE =' + this.getBuyId());
                var data = {selected_quantity: selection, product: product};
                return $http.post('http://localhost:9090/api/buy/buylineitems',data);
                /*if (buy == null) {
                    $http.get('http://localhost:9090/api/buys?username=salvatore&status=2')
                    .then(function successCallback(response) {
                        buy = response.data;
                        status = response.status;
                        console.log("backend loaded");
                        return buy;
                    }, function errorCallback(response){
                        if (response.data == null){
                            status=500;
                            buy="Server Unreachable";
                        }
                        else {
                            status = response.status;
                            buy = response.data;
                        }
                        return buy;
                    })
                }
                else {
                    status = 200;
                    return buy;
                }*/
            }

            BuyService.removeProduct = function(buyLineItemId) {
                console.log('REMOVE PRODUCT scope');
                console.log('id Buy SERVICE =' + this.getBuyId());
                //var data = {quantity: product.selected_quantity,price:product.price,product_id:product.id,product_name:product.name,buy_id:this.getBuyId()};
                return $http.delete('http://localhost:9090/api/buy/buylineitems/'+buyLineItemId);
                /*if (buy == null) {
                    $http.get('http://localhost:9090/api/buys?username=salvatore&status=2')
                    .then(function successCallback(response) {
                        buy = response.data;
                        status = response.status;
                        console.log("backend loaded");
                        return buy;
                    }, function errorCallback(response){
                        if (response.data == null){
                            status=500;
                            buy="Server Unreachable";
                        }
                        else {
                            status = response.status;
                            buy = response.data;
                        }
                        return buy;
                    })
                }
                else {
                    status = 200;
                    return buy;
                }*/
            }

            BuyService.endBuy = function(){
                return $http.get('http://localhost:9090/api/buy/buylineitems').then(
                    function(response){
                        bli = response.data;
                        return bli;
                    }
                );
                //bli = [{id:1,quantity:2,product:{id:6,name:"Sprite 1.5Litri",price:1.2,stock:null}},{id:2,quantity:3,product:{id:4,name:"Coca Cola 1.5Litri",price:1,stock:null}}];
                //return bli;
            }

            BuyService.cancelBuy = function(){
                return $http.delete('http://localhost:9090/api/buy').then(
                    function successCallback(response){
                        status = response.status;
                    }, function errorCallback(response){
                        status = response.status;
                    }
                );
            }

            BuyService.getBuyLineItems = function() {                
                    return $http.get('http://localhost:9090/api/buy/buylineitems');
            }

            BuyService.confirmBuy = function(){
                console.log("Confirm Buy Scope!");
                return $http.put('http://localhost:9090/api/buy/status/confirm').then(
                    function successCallback(response){
                        buy = response.data;
                        status = response.status;
                        console.log("Backend loaded - Buy Charged - BuyID="+buy.id);
                        console.log(buy);
                    }, function errorCallback(){
                        status = response.status;
                        console.log("Backend Error");
                        console.log(status);
                    }
                );
            };

            BuyService.inProgressBuy = function(){
                console.log("In Progress Buy Scope!");
                return $http.put('http://localhost:9090/api/buy/status/in_progress').then(
                    function successCallback(response){
                        buy = response.data;
                        status = response.status;
                        console.log("Backend loaded - Buy Charged - BuyID="+buy.id);
                        console.log(buy);
                    }, function errorCallback(){
                        status = response.status;
                        console.log("Backend Error");
                        console.log(status);
                    }
                );
            };

            BuyService.selectPickUpTime = function(time){
                console.log("SELECT PICK UP TIME SCOPE!");
                var data = time;
                console.log(data);
                return $http.put('http://localhost:9090/api/buy/shop/pickuptime',data).then(
                    function successCallback(response){
                        buy = response.data;
                        status = response.status;
                        console.log("Backend loaded - Buy Charged - BuyID="+buy.id);
                        console.log(buy);
                    }, function errorCallback(response){
                        status = response.status;
                        console.log("Backend Error");
                        console.log(status);
                    }
                );
            };

            BuyService.selectPayment = function(payment){
                console.log("SELECT PAYMENT SCOPE!");
                var data = payment;
                console.log(data);
                return $http.put('http://localhost:9090/api/buy/shop/payment',data).then(
                    function successCallback(response){
                        buy = response.data;
                        status = response.status;
                        console.log("Backend loaded - Buy Charged - BuyID="+buy.id);
                        console.log(buy);
                    }, function errorCallback(response){
                        status = response.status;
                        console.log("Backend Error");
                        console.log(status);
                    }
                );
            };

            BuyService.closeBuy = function(){
                console.log("Close Buy Scope!");
                return $http.put('http://localhost:9090/api/buy/status/close').then(
                    function successCallback(response){
                        buy = response.data;
                        status = response.status;
                        console.log("Backend loaded - Buy Charged - BuyID="+buy.id);
                        console.log(buy);
                    }, function errorCallback(){
                        status = response.status;
                        console.log("Backend Error");
                        console.log(status);
                    }
                );
            };

            BuyService.getBli = function(){
                return bli;
            }


            BuyService.getId = function () {
                $http.post('http://localhost:9090/api/buys').then(function(response) {
                    buy = response.data;
                });
                return buy;
            }
            
            BuyService.getBuy = function() {
                return buy;
            }



            BuyService.getStatus = function() {
                return status;
            }

            BuyService.setBuy = function(newbuy) {
                buy = newbuy;
            }

            return BuyService;

            
            /*
            BuyService.getOrCreateBuy = function () {
                return $http.post('http://localhost:9090/api/buys')
            }
            */

            /*
            BuyService.getBuy = function(){
                for(var key in buy) {
                    if(buy.hasOwnProperty(key))
                        return buy;
                }
                //return true;
                return $http.get('http://localhost:9090/api/buys?username=salvatore&status=1')
                .then(function(response) {
                    buy = response.data;
                    //console.log(buy);
                    return buy;
                });
                //return buy;
            };
            */
    });
