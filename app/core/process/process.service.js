angular.
    module('core.process').
    factory('Process',
        function($http,$route,$routeParams){
            
            var buy = {};

            var assignedBuys = [];
            var closedBuys = [];
            var processBuys = [];
            var readyBuys = [];
            var finishedBuys = [];
            var uncollectedBuys = [];

            var status = null;

            var bli = null;
            
            var ProcessService = {};

            ProcessService.getStatus = function() {
                return status;
            }

            ProcessService.getBuy = function() {
                return buy;
            }

            ProcessService.getRequestedClosedBuys = function() {
                return closedBuys;
            }

            ProcessService.getRequestedAssignedBuys = function() {
                return assignedBuys;
            }

            ProcessService.getRequestedReadyBuys = function() {
                return readyBuys;
            }

            ProcessService.getRequestedFinishedBuys = function() {
                return finishedBuys;
            }

            ProcessService.getRequestedUncollectedBuys = function() {
                return uncollectedBuys;
            }

            ProcessService.getClosedBuys = function(){
                 if (angular.equals([],closedBuys)) {
                     return this.refreshClosedBuys();
                    /*return $http.get('http://localhost:9090/api/processbuy/buys?status=CLOSED')
                        .then(function successCallback(response) {
                            closedBuys = response.data;
                            status = response.status;
                            console.log("Backend loaded - Closed Buys Charged");
                            console.log(closedBuys);
                            //return closedBuys;
                        }, function errorCallback(response){
                            status = response.status;
                            closedBuys = response.data;
                            //return closedBuys;
                        })*/
                }
                else {
                    console.log("Backend ALREADY loaded - Closed Buys Charged");
                    //status = 200;
                    //return closedBuys;
                }
            }

            ProcessService.refreshClosedBuys = function(){
                return $http.get('http://localhost:9090/api/processbuy/buys?status=CLOSED')
                    .then(function successCallback(response) {
                        closedBuys = response.data;
                        status = response.status;
                        console.log("Backend loaded - Closed Buys Charged");
                        console.log(closedBuys);
                        //return closedBuys;
                    }, function errorCallback(response){
                        status = response.status;
                        closedBuys = response.data;
                        //return closedBuys;
                    })
            }

            ProcessService.getRequestedBuy = function(buyId){
                return $http.get('http://localhost:9090/api/processbuy/buys/'+buyId)
                    .then(function successCallback(response) {
                        buy = response.data;
                        status = response.status;
                        console.log("Buys Charged");
                        console.log(buy);
                        console.log(status);
                        //return buy;
                    }, function errorCallback(response){
                        status = response.status;
                        buy = response.data;
                        //return buy;
                    })
            }

            ProcessService.assignAndRefresh = function(buyId){
                return this.assignBuy(buyId).then(
                    function(response){
                        return ProcessService.refreshClosedBuys();
                    }
                )
            }

            ProcessService.assignBuy = function(buyId){
                return $http.put('http://localhost:9090/api/processbuy/buys/'+buyId+'/operator/operator')
                    .then(function successCallback(response) {
                        buy = response.data;
                        status = response.status;
                        console.log("Buy Assigned");
                        console.log(buy);
                        console.log(status);
                        //return buy;
                    }, function errorCallback(response){
                        status = response.status;
                        buy = response.data;
                        //return buy;
                    })
            }

            ProcessService.getAssignedBuys = function(){
                if(angular.equals([],assignedBuys)){
                    return this.refreshAssignedBuys();
                }
                else{
                    console.log("Backend ALREADY loaded - Assigned Buys Charged");
                    //status = 200;
                    //return assignedBuys;
                }
            }

            ProcessService.refreshAssignedBuys = function(){
                return $http.get('http://localhost:9090/api/processbuy/buys?status=ASSIGNED')
                    .then(function successCallback(response){
                        assignedBuys = response.data;
                        status = response.status;
                        console.log("Backend loaded - Assigned Buys Charged");
                        console.log(assignedBuys);
                        //return assignedBuys;
                    }, function errorCallback(response){
                        assignedBuys = response.data;
                        status = response.status;
                        //return assignedBuys;
                    });
            }

            ProcessService.refuseAndRefresh = function(buyId){
                return this.refuseBuy(buyId).then(
                    function(response){
                        return ProcessService.refreshAssignedBuys();
                    }
                );
            }

            ProcessService.refuseBuy = function(buyId){
                return $http.delete('http://localhost:9090/api/processbuy/buys/'+buyId+'/operator/operator')
                    .then(function successCallback(response){
                        buy = response.data;
                        status = response.status;
                        console.log("Buy Refused");
                        console.log(buy);
                        console.log(status);
                        //return buy;
                    }, function errorCallback(response){
                        status = response.status;
                        buy = response.data;
                        //return buy;
                    });
            }

            ProcessService.processAndRefresh = function(buyId){
                return this.processBuy(buyId).then(
                    function(response){
                        return ProcessService.refreshAssignedBuys();
                    }
                );
            }

            ProcessService.processBuy = function(buyId){
                return $http.put('http://localhost:9090/api/processbuy/buys/'+buyId+'/status/process')
                    .then(function successCallback(response){
                        buy = response.data;
                        status = response.status;
                        console.log('Success callback scope');
                    }, function errorCallback(response){
                        buy = response.data;
                        status = response.status;
                    });
            }

            ProcessService.getProcessBuys = function(){
                return $http.get('http://localhost:9090/api/processbuy/buys?status=PROCESS')
                    .then(function successCallback(response) {
                        processBuys = response.data;
                        buy = processBuys[0];
                        status = response.status;
                        console.log("Backend loaded - Process Buys Charged");
                        console.log(processBuys);
                        //return closedBuys;
                    }, function errorCallback(response){
                        status = response.status;
                        processBuys = response.data;
                        //return closedBuys;
                    })
            }

            ProcessService.readyBuy = function(buyId){
                return $http.put('http://localhost:9090/api/processbuy/buys/'+buyId+'/status/ready')
                    .then(function successCallback(response){
                        buy = response.data;
                        status = response.status;
                        console.log('Success callback scope');
                    }, function errorCallback(response){
                        buy = response.data;
                        status = response.status;
                    });
            }

            ProcessService.getReadyBuys = function(){
                 if (angular.equals([],readyBuys)) {
                     return this.refreshReadyBuys();
                }
                else {
                    console.log("Backend ALREADY loaded - Ready Buys Charged");
                    //status = 200;
                    //return closedBuys;
                }
            }

            ProcessService.refreshReadyBuys = function(){
                return $http.get('http://localhost:9090/api/processbuy/buys?status=READY')
                    .then(function successCallback(response) {
                        readyBuys = response.data;
                        status = response.status;
                        console.log("Backend loaded - Ready Buys Charged");
                        console.log(readyBuys);
                        //return closedBuys;
                    }, function errorCallback(response){
                        status = response.status;
                        readyBuys = response.data;
                        //return closedBuys;
                    })
            }

            ProcessService.finishAndRefresh = function(buyId){
                return this.finishBuy(buyId).then(
                    function(response){
                        return ProcessService.refreshReadyBuys();
                    }
                );
            }

            ProcessService.finishBuy = function(buyId){
                return $http.put('http://localhost:9090/api/processbuy/buys/'+buyId+'/status/finish')
                    .then(function successCallback(response){
                        buy = response.data;
                        status = response.status;
                        console.log('Success callback scope');
                    }, function errorCallback(response){
                        buy = response.data;
                        status = response.status;
                    });
            }

            ProcessService.uncollectedBuy = function(buyId){
                return $http.put('http://localhost:9090/api/processbuy/buys/'+buyId+'/status/uncollected')
                    .then(function successCallback(response){
                            buy = response.data;
                            status = response.status;
                            console.log('Success callback scope');
                        }, function errorCallback(response){
                            buy = response.data;
                            status = response.status;
                        });
            }

            ProcessService.uncollectedAndRefresh = function(buyId){
                return this.uncollectedBuy(buyId)
                    .then(function(response){
                        return ProcessService.refreshReadyBuys();
                    })
            };

            ProcessService.getFinishedBuys = function(){
                 if (angular.equals([],finishedBuys)) {
                     return this.refreshFinishedBuys();
                }
                else {
                    console.log("Backend ALREADY loaded - Finished Buys Charged");
                    //status = 200;
                    //return closedBuys;
                }
            }

            ProcessService.refreshFinishedBuys = function(){
                return $http.get('http://localhost:9090/api/processbuy/buys?status=FINISHED')
                    .then(function successCallback(response) {
                        finishedBuys = response.data;
                        status = response.status;
                        console.log("Backend loaded - Finished Buys Charged");
                        console.log(finishedBuys);
                        //return closedBuys;
                    }, function errorCallback(response){
                        status = response.status;
                        finishedBuys = response.data;
                        //return closedBuys;
                    })
            }

            ProcessService.getUncollectedBuys = function(){
                 if (angular.equals([],uncollectedBuys)) {
                     return this.refreshUncollectedBuys();
                }
                else {
                    console.log("Backend ALREADY loaded - Uncollected Buys Charged");
                    //status = 200;
                    //return closedBuys;
                }
            }

            ProcessService.refreshUncollectedBuys = function(){
                return $http.get('http://localhost:9090/api/processbuy/buys?status=UNCOLLECTED')
                    .then(function successCallback(response) {
                        uncollectedBuys = response.data;
                        status = response.status;
                        console.log("Backend loaded - Uncollected Buys Charged");
                        console.log(uncollectedBuys);
                        //return closedBuys;
                    }, function errorCallback(response){
                        status = response.status;
                        uncollectedBuys = response.data;
                        //return closedBuys;
                    })
            }

            return ProcessService;
    });
