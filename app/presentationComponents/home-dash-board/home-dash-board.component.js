angular.
  module('presentationComponents.homeDashBoard').
  component('homeDashBoard', {
    templateUrl: 'presentationComponents/home-dash-board/home-dash-board.template.html',
    controller: [ 'Process', 
      function HomeDashBoardController( Process ) {

        var self = this;
        self.user = 'Salvatore';

        self.goToConsumerTutorial = function(){
          window.location.replace('#!/dashboard');
        }

        self.goToOperatorTutorial = function(){
          window.location.replace('#!/operator');
        }

      }
    ]
  });